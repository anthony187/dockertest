from django.shortcuts import render, redirect
from alphabet_checker.classes import *
# Create your views here.



def alphabet_checker(request):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    
    length = len(alphabet)
    number = len(alphabet)
    used = 0
    if request.method == "POST":
        word = request.POST["word"]
        if word == "":
            word = "Type in a word!"
        else:
            checker = AlphabetChecker(word,alphabet)
            alphabet, number = checker.cross_check()
            
        number = len(alphabet)
        used = length-number
    else:
        word = "Type in a word!"
    return render(request,"alphabet_checker/alphabet_checker.html",{"word":word, "alphabet":alphabet,"number":number,"used":used})