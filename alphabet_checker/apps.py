from django.apps import AppConfig


class AlphabetCheckerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'alphabet_checker'
